﻿open System
open System.Diagnostics
open Microsoft.FSharp.Math
open Microsoft.FSharp.Collections

let inline dup x = x, x
let inline passthrough f x = f x; x

type Ops1 =
    | Const
    | Neg
type Ops2 =
    | Plus
    | Minus
    | Mult
    | Div
    | Concat

type Expr<'T> =
    | Const  of 'T
    | Neg    of Expr<'T>
    | Plus   of Expr<'T> * Expr<'T>
    | Minus  of Expr<'T> * Expr<'T>
    | Mult   of Expr<'T> * Expr<'T>
    | Div    of Expr<'T> * Expr<'T>
    | Concat of Expr<'T> * Expr<'T>

let decimals = Seq.unfold (fun last -> last * 10 |> dup |> Some ) 1
let findRange f1 f2 v =
    let ix = f1 v
    Seq.pick (fun x -> if ix < x then Some x else None) decimals // |> BigRational.FromInt
    |> f2

let findRangeInt = findRange id id
let findRangeFloat = findRange int float
let findRangeBigRational = findRange Math.BigRational.ToInt32 Math.BigRational.FromInt

let rec calcInt = function
    | Const x       -> x
    | Neg   x       -> - calcInt x
    | Plus (x, y)   -> calcInt x + calcInt y
    | Minus (x, y)  -> calcInt x - calcInt y
    | Mult (x, y)   -> calcInt x * calcInt y
    | Div (x, y)    -> calcInt x / calcInt y
    | Concat (x, y) ->
                       let v = calcInt y
                       calcInt x * (findRangeInt v) + v

let rec calcFloat = function
    | Const x       -> x
    | Neg   x       -> - calcFloat x
    | Plus (x, y)   -> calcFloat x + calcFloat y
    | Minus (x, y)  -> calcFloat x - calcFloat y
    | Mult (x, y)   -> calcFloat x * calcFloat y
    | Div (x, y)    -> calcFloat x / calcFloat y
    | Concat (x, y) ->
                       let v = calcFloat y
                       calcFloat x * (findRangeFloat v) + v

let rec calcBigRational = function
    | Const x       -> x
    | Neg   x       -> - calcBigRational x
    | Plus (x, y)   -> calcBigRational x + calcBigRational y
    | Minus (x, y)  -> calcBigRational x - calcBigRational y
    | Mult (x, y)   -> calcBigRational x * calcBigRational y
    | Div (x, y)    -> calcBigRational x / calcBigRational y
    | Concat (x, y) ->
                       let v = calcBigRational y
                       calcBigRational x * (findRangeBigRational v) + v

(**)
let split (xs: 'T array) =
    let len = (Seq.length xs) - 1
    seq {
        for i in 1..len do
            yield xs.[0..i-1], xs.[i..len]
    }

let rec candidates digits (ops1: Ops1 seq) (ops2: Ops2 seq) undepth : Expr<'T> seq =
    seq {
        if undepth <> 0 then
            for op1 in ops1 do
                match op1 with
                | Ops1.Const -> if Array.length digits = 1 then yield digits.[0] |> Const
                | Ops1.Neg   -> yield! (candidates digits ops1 ops2 (undepth-1) |> Seq.map Neg)
        for p1, p2 in split digits do
            let es1 = candidates p1 ops1 ops2 undepth
            let es2 = candidates p2 ops1 ops2 undepth
            for e1 in es1 do
            for e2 in es2 do
            for op2 in ops2 do
                match op2 with
                | Ops2.Plus  -> yield Plus (e1, e2)
                | Ops2.Minus -> yield Minus (e1, e2)
                | Ops2.Mult  -> yield Mult (e1, e2)
                | Ops2.Div   -> yield Div (e1, e2)
                | Ops2.Concat ->
                    match e1, e2 with
                    | Const _,  Const _
                    | Const _,  Concat _
                    | Concat _, Const _
                    | Concat _, Concat _ -> yield Concat (e1, e2)
                    | _                  -> ()
    }

Concat(Const 1N, Const 0N) |> calcBigRational |> float |> printfn "%f = 10"
Concat(Const 4N, Const 2N) |> calcBigRational |> float |> printfn "%f = 42"
Concat(Const 10N, Const 23N) |> calcBigRational |> float |> printfn "%f = 1023"
Concat(Const 1N, Concat(Const 2N, Const 3N))  |> calcBigRational |> float|> printfn "%f = 123"
Concat(Const 5N, Concat(Concat(Const 7N, Const 8N), Const 3N)) |> calcBigRational|> float|> printfn "%f = 5783"
Concat(Concat(Const 3N, Const 2N), Concat(Concat(Const 7N, Const 8N), Const 3N)) |> calcBigRational |> float |> printfn "%f = 3283"

-12345N |> float |> printfn "%f = -12345"

let makeString<'T>(* makeStringInt, makeStringBigInt*) =
    let rec makeString' lvl =
        function
        | Const (x:'T)       -> 9, x.ToString()// sprintf "%d" (int x)
        | Concat (x, y) -> 8, (makeString' 8 x)       + (makeString' 8 y)
        | Neg   x       -> 2,                     "-" + (makeString' 1 x)
        | Plus (x, y)   -> 3, (makeString' 3 x) + "+" + (makeString' 3 y)
        | Minus (x, y)  -> 4, (makeString' 3 x) + "-" + (makeString' 5 y)
        | Mult (x, y)   -> 5, (makeString' 5 x) + "*" + (makeString' 5 y)
        | Div (x, y)    -> 6, (makeString' 5 x) + "/" + (makeString' 7 y)
        >> fun (lvlThis, v) ->
            if lvlThis >= lvl
            then v
            else sprintf "(" + v + ")"
    makeString' 0

let v1int() = Plus(Const 1, Mult(Const 5, Concat(Const 10, Const 9)))
let v1bigrat() = Plus(Const 1N, Mult(Const 5N, Concat(Const 10N, Const 9N)))

let bld f x =
    Seq.init 10000 (fun _ -> x())
    //|> Seq.map f
    |> Seq.map makeString
    |> Seq.length
    |> ignore

let timer0 = new Stopwatch()
timer0.Start()
bld calcInt v1int
timer0.Stop()
printfn "%A" <| timer0.Elapsed

timer0.Reset()
timer0.Start()
bld calcBigRational v1bigrat
timer0.Stop()
printfn "%A" <| timer0.Elapsed

let digits0 = [|1; 2; 3; 4; 5; 6; 7; 8|]
let digits2 = [|10..-1..0|]
let ops1_0 = [Const]
let ops2_0 = [Plus; Minus; Mult; Div]

let digits1 = [|10N .. -1N .. 0N|] // |> Array.map BigRational.FromInt
let ops1_1 = [Ops1.Const]
let ops2_1 = [Ops2.Concat; Ops2.Plus; Ops2.Minus; (**)Ops2.Mult;(**) (**)Ops2.Div(**)]

split digits0
|> Seq.map (fun (xs, ys) -> sprintf "%A | %A" xs ys)
|> Seq.iter (printfn "%s")

let filter mut f xs =    
    seq {
        for x in xs do
            mut := !mut + 1
            if f x then yield x
    }

let countTotal = ref 0L
let countGood = ref 0L
let countUnique = ref 0L

let resultsAll = ref Set.empty<string>
let results2 = ref List.empty<string>
let resultsLength = new Collections.Generic.SortedDictionary<int, string list>()
let resultsComplexity = new Collections.Generic.SortedDictionary<int, string list>()

let timer = new Stopwatch()

let output _ =
    Console.Clear()
    // best by length
    Console.SetCursorPosition(0, 0)
    lock resultsLength
        (fun () ->
            printf "Best by length (%d items)" resultsLength.Count
            resultsLength
            |> Seq.collect (fun x -> x.Value |> Seq.map (fun v -> x.Key, v))
            |> Seq.truncate 20
            |> Seq.iteri (fun i (k,v) ->
                            Console.SetCursorPosition(0, i+1)
                            printf "%d:%s" k v
                         )
        )

    // best by parens
    Console.SetCursorPosition(40, 0)
    lock resultsComplexity
        (fun () ->
            printf "Best by parens (%d items)" resultsComplexity.Count
            resultsComplexity
            |> Seq.collect (fun x -> x.Value |> Seq.map (fun v -> x.Key, v))
            |> Seq.truncate 20
            |> Seq.iteri (fun i (k,v) ->
                            Console.SetCursorPosition(40, i+1)
                            printf "%d:%s" k v
                         )
        )
    // recent
    Console.SetCursorPosition(0, 22)
    printf "---------------------------------------------------------"
    Console.SetCursorPosition(0, 23)
    printf "Recent %s of %s of %s, working %s, %s hash/sec"
        (String.Format("{0:#,###}", !countUnique))
        (String.Format("{0:#,###}", !countGood))
        (String.Format("{0:#,###}", !countTotal))
        (timer.Elapsed.TotalSeconds |> Math.Floor |> TimeSpan.FromSeconds |> string)
        (String.Format("{0:#,###}", (float(!countTotal) / timer.Elapsed.TotalSeconds |> int)))
    lock results2
        (fun () ->
            !results2
            |> Seq.truncate 10
            |> Seq.toList
            |> List.rev
            |> Seq.iteri (fun i x ->
                            Console.SetCursorPosition(0, i+24)
                            printf "%s" x
                         )
        )

let addToDict (dict:Collections.Generic.SortedDictionary<_, _>) max key value =
    if dict.ContainsKey key
    then dict.[key] <- value :: dict.[key]
    else dict.Add(key, [value])
    if dict.Count < max then () else ()

let cpx x =
    let cpx' s x =
        Seq.fold (fun (curr, level) -> function
                    | '(' -> curr+level, level + 1
                    | ')' -> curr, level - 1
                    | _ -> curr, level
                 ) s x
    cpx' (0, 1) x
    |> fst
    
let comparerBigRational x =
    try
        calcBigRational x = 2014N
    with
        _ -> false
    //Math.Abs (calc x - 2014.) < 0.00001

let comparerInt x =
    try
        calcInt x = 2014
    with
        _ -> false

let comparerFloat x =
    try
        Math.Abs(calcFloat x - 2014.) < 0.00001
    with
        _ -> false


let trigger = new Timers.Timer (3000.)
trigger.Elapsed.Add output
trigger.Start()

timer.Start()
//let proc() =
candidates ((Array.sub digits2 0 11) |> Array.map float) ops1_1 ops2_1 1
|> PSeq.map (passthrough (fun _ -> countTotal := !countTotal + 1L))
|> PSeq.length
|> printf "%d"

Seq.empty
|> PSeq.filter comparerFloat
|> PSeq.map makeString
|> PSeq.map (passthrough (fun x -> countGood := !countGood + 1L))
|> PSeq.filter (fun x -> lock resultsAll (fun () -> if Set.contains x !resultsAll then false else resultsAll := Set.add x !resultsAll; true))
|> PSeq.map (passthrough (fun x -> countUnique := !countUnique + 1L))
|> PSeq.map (fun x -> lock results2 (fun () -> results2 := (x :: !results2) |> Seq.truncate 10 |> Seq.toList; x))
|> PSeq.map (fun x -> lock resultsLength (fun () -> let len = x.Length in addToDict resultsLength 10 len x; x))
|> PSeq.map (fun x -> lock resultsComplexity (fun () -> let cpx = cpx x in addToDict resultsComplexity 10 cpx x;  x))
//|> Seq.iter (*printfn "%A"*) output
//|> PSeq.withDegreeOfParallelism 8
//|> PSeq.withMergeOptions Linq.ParallelMergeOptions.NotBuffered
|> PSeq.iter ignore
|> ignore

//proc()

trigger.Stop()
timer.Stop()
printfn "\n%s records in %A, %d hash/sec" (String.Format("{0:#,###}", !countTotal)) timer.Elapsed (float !countTotal / timer.Elapsed.TotalSeconds |> int)
let zz = 0
